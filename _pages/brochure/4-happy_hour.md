---
title:  "Happy Hour"
image:  "happy_hour"
ref: "happy_hour"
categories: "brochure.md"
id: 4
---

# Value: R$250

# Benefits

At FLUSP we prize social interactions. For this reason, we will have a happy
hour with the attendees after event completion. At this moment will be offered
the last publicity standing for sponsors to display banners or distribute
fliers.
